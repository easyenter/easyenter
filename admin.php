<!DOCTYPE html>
<head>
    <title>EasyEnter</title>
    <link rel="stylesheet" type="text/css" href="css/global.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
     integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	 <link rel="stylesheet" type="text/css" href="css/responsiveTable.css">
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body style="background-color: #ecf0f1">
  <!--Navigation bar-->
  <div id="navigation"></div>

  <script>
  $(function(){
    $("#navigation").load("config/admin_navigation.php");
  });
  </script>
  <!--end of Navigation bar-->

  <?php
    session_start();
    if(!isset($_SESSION['user'])){
        header("location:login.php");
    }

    include 'config/sql_config.php';
    // Create connection
    $conn = new mysqli($host, $username, $password, $database);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if(isset($_GET['delete'])) {
         $id = $_GET[('delete')];
         $conn->query("DELETE FROM Users WHERE id=$id");
     }
  ?>

  <!--Welcome message-->
  <div style="width: 100%; height: 28px; background-color: #454d55;">
    <span class="badge badge-dark" style="margin-left: 15px; padding: 5px;">
      <?php echo 'Welcome ' . $_SESSION['user']; ?>
    </span>
  </div>
  <!--End of welcome message-->

  <div class="container">
    <div class="row">
      <div class="col-sm">

        <table class="table table-striped table-dark">
          <thead>
            <hr><h4 style="text-align: center">User status and control</h4><hr>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Surname</th>
              <th scope="col">Card UID</th>
              <th scope="col">Status</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $sql = "SELECT *, (SELECT cameIn FROM Arduino_Data WHERE uid = Users.uid ORDER BY id DESC LIMIT 1) AS cameIn FROM Users WHERE 1";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                  echo "<tr>
                    <th scope='row'>".$row["id"]."</th>
                    <td>".$row["firstName"]."</td>
                    <td>".$row["lastName"]."</td>
                    <td>".$row["uid"]."</td>
                    <td>";
                    if($row['cameIn'] === null || $row['isActive'] == 0)
                      echo "<span class='badge badge-dark'>No Info</span>";
            				elseif($row['cameIn'] == 1)
            					echo "<span class='badge badge-success'>Active</span>";
            				elseif($row['cameIn'] == 0)
                      echo "<span class='badge badge-danger'>Not Active</span>";
                    echo "</td>
                    <td class='btn-group'>
                      <a style='margin-right: 10px' href='../edit_form.php?edit=".$row["id"]."'>
                        <button type='submit' class='btn btn-outline-info btn-sm' style='height: 27px;'>
                          <span class='material-icons' style='font-size: 15px;'>settings</span>
                        </button>
                      </a>
                      <a href='../admin.php?delete=".$row["id"]."'>
                        <button type='submit' class='btn btn-outline-danger btn-sm' style='height: 27px;'>
                          <span class='material-icons' style='font-size: 15px;'>highlight_off</span>
                        </button>
                      </a>
                    </td>
                  </tr>";
                }
              }
              ?>
          </tbody>
        </table>

      </div>
      <div class="col-sm">
        <table class="table table-striped table-dark">
          <thead>
            <hr><h4 style="text-align: center">Data from card reader</h4><hr>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Card UID</th>
              <th scope="col">Status</th>
              <th scope="col">Date</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $sql = "SELECT * FROM Arduino_Data";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                  echo "<tr>
                    <th scope='row'>".$row["id"]."</th>
                    <td>".$row["uid"]."</td>
                    <td>".$row["cameIn"]."</td>
                    <td>".$row["date"]."</td>";
                }
              }
              ?>
          </tbody>
        </table>
      </div>

    </div>
  </div>


  <?php $conn->close(); ?>
</body>
</html>
