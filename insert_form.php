<!DOCTYPE html>
<head>
    <title>EasyEnter</title>
    <link rel="stylesheet" type="text/css" href="css/global.css"> 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
     integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<?php
    session_start();
    if(isset($_SESSION['user']))
    {
        
        
    }
    else
    {
        header("location:login.php");
    }
?>

<body>
<div>  
<section class="containter-fluid">
    <nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
    <img src="images/EasyEnterLogo.jpg"
         width="60" height="35" class="d-inline-block align-top" alt="">
         <?php echo ' Welcome ' . $_SESSION['user']; ?>
    </a>
    </button>
    <button type="button" class="btn btn-light">
        <a class="button" name="logout" href="logout.php?logout">Atsijungti</a>
    </button>
    
    </nav>
      
<div class="br">
	<div class="jumbotron bg-light" style="width: 450px; text-align: left">
		<form action="insert.php" method="post">
			<h1>Enter user info:</h1>
			<input hidden type="text" name="plzDontDelete" value="kad graziau atrodytu">
				</br>
			<div class="form-group row">
				<label for="firstNameField" class="col-form-label col-md-5">First Name:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="firstNameField" name="firstName">
				</div>
			</div>
			<div class="form-group row">
				<label for="lastNameField" class="col-form-label col-md-5">Last Name:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="lastNameField" name="lastName">
				</div>
			</div>
			<div class="form-group row">
				<label for="uidField" class="col-form-label col-md-5">Card Number:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="uidField" name="uid">
				</div>
			</div>
			<div class="form-group row">
				<label for="usernameField" class="col-form-label col-md-5">Username:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="usernameField" name="username">
				</div>
			</div>
			<div class="form-group row">
				<label for="passwordField" class="col-form-label col-md-5">Password:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="passwordField" name="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="isAdminField" class="col-form-label col-md-5">Is Admin:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="isAdminField" name="isAdmin">
				</div>
			</div>
			<div class="form-group row">
				<label for="isActiveField" class="col-form-label col-md-5">Is Active:</label>
				<div class="col-md">
					<input type="text" class="form-control" id="isActiveField" name="isActive">
				</div>
			</div>
			<input class="btn btn-primary" type="submit" value="Insert" href="admin.php">
        </form>
	</div>
</div>
</div>
</body>
</html>