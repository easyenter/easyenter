<nav class='navbar navbar-expand-lg navbar-dark bg-dark'>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarTogglerDemo03' aria-controls='navbarTogglerDemo03' aria-expanded='false' aria-label='Toggle navigation'>
    <span class='navbar-toggler-icon'></span>
  </button>
  <a class='navbar-brand' href='../admin.php'>
    <img src="../images/easyenter.png" height="35" alt="">
  </a>

  <div class='collapse navbar-collapse' id='navbarTogglerDemo03'>
    <ul class='navbar-nav mr-auto mt-2 mt-lg-0'>
      <li class='nav-item'>
        <a class='nav-link' href='../admin.php'><span class="material-icons" style="font-size: 18px; vertical-align: middle; margin-bottom: 3px;">dashboard</span> Dashboard</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='../insert_form.php'><span class="material-icons" style="font-size: 18px; vertical-align: middle; margin-bottom: 3px;">add_box</span> New user</a>
      </li>
    </ul>
    <form class='form-inline my-2 my-lg-0' action="../logout.php?logout" method="post">
      <button type="submit" class="btn btn-outline-info btn-sm">Logout</button>
    </form>
  </div>
</nav>
