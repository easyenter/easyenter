<!DOCTYPE html>
<html>
<head>
    <title>EasyEnter</title>
    <link rel="stylesheet" type="text/css" href="css/global.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
     integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
    
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
    <img src="images/EasyEnterLogo.jpg"
         width="60" height="35" class="d-inline-block align-top" alt="">
    EasyEnter
    </a>  
    <button type="button" class="btn btn-light">
        <a class="button" href="about.php">Apie mus</a>
    </button>
    <button type="button" class="btn btn-light">
        <a class="button" href="login.php">Prisijungti</a>
    </button>
</nav>
<header>
    <div class="br">
    

<br>
<br>
<br>
<br>
<br>

<div class="padding">
<div class="container card home" style="width: 65rem ; padding: auto; padding-top: 30px; padding-bottom: 20px; background-color: #cccccc;">
    <div class="row">
        <div class="col-sm-6">
            <img src="images/office1.jpeg" class="img-responsive">
        </div>
        <div class="col-sm-6 text-center">
            <h2>All you need to know</h2>
            <p class="lead">
             If you are having a hard time of tracking your workers, contact us and we will help your company to install/create tracking system that
              let's you track your employers. When they enter or leave their working area. Sounds good? Then join us at EasyEnter.
             
            </p>
            <p class="lead">
                So if you are sure, we will give you constant 24/7 support
            </p>
        </div>
    </div>
</div>
</div>
</div>
  </div>
</header>



</body>
</html>